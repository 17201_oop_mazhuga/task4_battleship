//
// Created by 214 on 09.12.2018.
//

#include "Coordinates.h"

Coordinates::Coordinates(unsigned x0, unsigned y0) : x(x0), y(y0) {
    //if (x0 > 9 || y0 > 9)
    //    throw BattleshipException("Wrong coordinates");
}

std::ostream &operator<<(std::ostream &out, const Coordinates &crd) {
    out << (char)('A' + crd.x) << ' ' << crd.y;
    return out;
}

bool Coordinates::isBorder() const {
    return x == 0 || x == 9 || y == 0 || y == 9;
}

bool Coordinates::isCorner() const {
    return (x == 0 || x == 9) && (y == 0 || y == 9);
}

bool Coordinates::operator==(const Coordinates &other) const {
    return x == other.x && y == other.y;
}

bool Coordinates::operator!=(const Coordinates &other) const {
    return !(*(this) == other);
}

Coordinates Coordinates::topLeft() const {
    return Coordinates(x - 1, y - 1);
}

Coordinates Coordinates::topCentral() const {
    return Coordinates(x, y - 1);
}

Coordinates Coordinates::topRight() const {
    return Coordinates(x + 1, y - 1);
}

Coordinates Coordinates::centralLeft() const {
    return Coordinates(x - 1, y);
}

Coordinates Coordinates::centralRight() const {
    return Coordinates(x + 1, y);
}

Coordinates Coordinates::bottomLeft() const {
    return Coordinates(x - 1, y + 1);
}

Coordinates Coordinates::bottomCentral() const {
    return Coordinates(x, y + 1);
}

Coordinates Coordinates::bottomRight() const {
    return Coordinates(x + 1, y + 1);
}