//
// Created by 214 on 09.12.2018.
//

#include <iostream>
#include <cstdlib>
#include <ctime>

#include "Coordinates.h"

#ifndef INC_04_BATTLESHIP_FIELD_H
#define INC_04_BATTLESHIP_FIELD_H

enum class fieldCell {
    Empty = '0',
    Unknown = '.',
    Ship = '1',
    Hit = 'X',
    Dead = 'D',
    Error = 'E'
};

enum class shipOrientation {
    Vertical = 0, Horizontal = 1
};

enum class Direction {      //can be extended later if needed, only used in findCellAround function
    All = 0, LeftAndBottom, NoCorners
};

class Field {
private:
    fieldCell field[10][10];
public:
    Field(fieldCell fillValue);

    //field is inverted!!!

    fieldCell getCell(const Coordinates crd) const;
    bool setCell(const Coordinates crd, const fieldCell value);

    Coordinates findCell(const fieldCell target) const;
    Coordinates findCellAround(const fieldCell target, const Coordinates area, const Direction dir = Direction::All) const;
    Coordinates getRandomCellOfType(const fieldCell target) const;

    bool canCellFitShip(const Coordinates crd, const unsigned size, const shipOrientation orientation);
    bool setShipToCell(const Coordinates crd, const unsigned size, const shipOrientation orientation = shipOrientation::Horizontal);
    void markKilled(const Coordinates crd);

    void print() const;
};


#endif //INC_04_BATTLESHIP_FIELD_H
