//
// Created by 214 on 09.12.2018.
//

#include <iostream>

#ifndef INC_04_BATTLESHIP_COORDINATES_H
#define INC_04_BATTLESHIP_COORDINATES_H

struct Coordinates {
    unsigned  x, y;

    Coordinates(unsigned x0 = 0, unsigned y0 = 0);

    bool isBorder() const;
    bool isCorner() const;

    Coordinates topLeft() const;
    Coordinates topCentral() const;
    Coordinates topRight() const;
    Coordinates centralLeft() const;
    Coordinates centralRight() const;
    Coordinates bottomLeft() const;
    Coordinates bottomCentral() const;
    Coordinates bottomRight() const;

    bool operator==(const Coordinates &other) const;
    bool operator!=(const Coordinates &other) const;
    friend std::ostream& operator<<(std::ostream &out, const Coordinates &crd);
};
static const Coordinates errorCoordinates(10, 10);


#endif //INC_04_BATTLESHIP_COORDINATES_H
