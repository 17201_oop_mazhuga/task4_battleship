//
// Created by d214 on 15/12/18.
//

#include "AIBase.h"

#ifndef INC_04_BATTLESHIP_AIDIAGONAL_H
#define INC_04_BATTLESHIP_AIDIAGONAL_H

class AIDiagonal : public AIBase{
    Coordinates finish(const Coordinates crdHit, const shipOrientation orient = shipOrientation::Horizontal) const;

    void makeField() override;
    Coordinates shoot() const override;
};


#endif //INC_04_BATTLESHIP_AIDIAGONAL_H
