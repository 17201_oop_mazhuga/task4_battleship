//
// Created by 214 on 08.12.2018.
//

#include "AIAllRandom.h"

Coordinates AIAllRandom::shoot() const {
    return enemyField.getRandomCellOfType(fieldCell::Unknown);
}

void AIAllRandom::makeField() {
    Coordinates crd;
    shipOrientation orient;
    unsigned shipCounter;

    for (unsigned size = 4; size >= 1; size--) {
        shipCounter = 0;

        while (shipCounter < 5 - size) {
            crd = myField.getRandomCellOfType(fieldCell::Empty);
            orient = (shipOrientation)(rand() % 2);

            if (myField.setShipToCell(crd, size, orient))
                shipCounter++;
        }
    }
}

AIAllRandom::AIAllRandom() {

}
