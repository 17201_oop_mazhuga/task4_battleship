//
// Created by 214 on 08.12.2018.
//

#include <iostream>
#include <fstream>

#include "BattleshipException.h"
#include "Field.h"

#ifndef INC_04_BATTLESHIP_TACTIC_H
#define INC_04_BATTLESHIP_TACTIC_H

class AIBase {
protected:
    Field myField, enemyField;

    AIBase();
    virtual void makeField() = 0;
    virtual Coordinates shoot() const = 0;
public:
    virtual ~AIBase(){};
    void start();
};


#endif //INC_04_BATTLESHIP_TACTIC_H
