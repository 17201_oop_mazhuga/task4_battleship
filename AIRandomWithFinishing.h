//
// Created by d214 on 10/12/18.
//

#include <cstdlib>
#include <ctime>

#include "AIBase.h"

#ifndef INC_04_BATTLESHIP_AIRANDOMWITHFINISHING_H
#define INC_04_BATTLESHIP_AIRANDOMWITHFINISHING_H


class AIRandomWithFinishing : public AIBase{
private:
    Coordinates finish(const Coordinates crdHit, const shipOrientation orient = shipOrientation::Horizontal) const;

    void makeField() override;
    Coordinates shoot() const override;
public:
    AIRandomWithFinishing();
};


#endif //INC_04_BATTLESHIP_AIRANDOMWITHFINISHING_H
