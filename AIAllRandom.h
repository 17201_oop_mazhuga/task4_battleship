//
// Created by 214 on 08.12.2018.
//

#include <cstdlib>
#include <ctime>

#include "AIBase.h"

#ifndef INC_04_BATTLESHIP_TACTICFULLRANDOM_H
#define INC_04_BATTLESHIP_TACTICFULLRANDOM_H

class AIAllRandom : public AIBase {
private:
    void makeField() override;
    Coordinates shoot() const override;
public:
    AIAllRandom();
};


#endif //INC_04_BATTLESHIP_TACTICFULLRANDOM_H
