//
// Created by 214 on 08.12.2018.
//

#include <exception>
#include <string>

#ifndef INC_04_BATTLESHIP_BATTLESHIPEXCEPTION_H
#define INC_04_BATTLESHIP_BATTLESHIPEXCEPTION_H

class BattleshipException : std::exception {
private:
    std::string msg;
public:
    explicit BattleshipException(const std::string s) : msg(s) {}
    const char* what() const _GLIBCXX_USE_NOEXCEPT override {
        return msg.data();
    }
};

#endif //INC_04_BATTLESHIP_BATTLESHIPEXCEPTION_H
