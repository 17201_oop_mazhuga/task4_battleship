//
// Created by 214 on 09.12.2018.
//

#include "Field.h"

fieldCell Field::getCell(const Coordinates crd) const {
    if (crd.x < 0 || crd.y < 0 || crd.x > 9 || crd.y > 9)
        return fieldCell::Error;

    return field[crd.y][crd.x];
}

bool Field::setCell(const Coordinates crd, const fieldCell value) {
    if (crd.x < 0 || crd.y < 0 || crd.x > 9 || crd.y > 9)
        return false;

    field[crd.y][crd.x] = value;

    return true;
}

void Field::print() const {
    std::string str;
    for (int i = 0; i < 10; i++) {
        for (int j = 0; j < 10; j++) {
            str += (char) field[i][j];
        }
        str += '\n';
    }
    std::cout << str;
}

Coordinates Field::findCell(const fieldCell target) const {
    for (int i = 0; i < 10; i++) {
        for (int j = 0; j < 10; j++) {
            if (field[i][j] == target)
                return Coordinates(j, i);
        }
    }

    return errorCoordinates;
}

Coordinates Field::findCellAround(const fieldCell target, const Coordinates area, const Direction dir) const {
    if (dir == Direction::All) {
        if (getCell(area.topLeft()) == target)
            return area.topLeft();
        else if (getCell(area.topCentral()) == target)
            return area.topCentral();
        else if (getCell(area.topRight()) == target)
            return area.topRight();
        else if (getCell(area.centralLeft()) == target)
            return area.centralLeft();
        else if (getCell(area.centralRight()) == target)
            return area.centralRight();
        else if (getCell(area.bottomLeft()) == target)
            return area.bottomLeft();
        else if (getCell(area.bottomCentral()) == target)
            return area.bottomCentral();
        else if (getCell(area.bottomRight()) == target)
            return area.bottomRight();
    }
    else if (dir == Direction::LeftAndBottom){
        if (getCell(area.centralRight()) == target)
            return area.centralRight();
        else if (getCell(area.bottomCentral()) == target)
            return area.bottomCentral();
        else if (getCell(area.bottomRight()) == target)
            return area.bottomRight();
    }
    else if (dir == Direction::NoCorners) {
        if (getCell(area.topCentral()) == target)
            return area.topCentral();
        else if (getCell(area.centralLeft()) == target)
            return area.centralLeft();
        else if (getCell(area.centralRight()) == target)
            return area.centralRight();
        else if (getCell(area.bottomCentral()) == target)
            return area.bottomCentral();
    }

    return errorCoordinates;
}

void Field::markKilled(const Coordinates crd) {
    setCell(crd, fieldCell::Dead);

    Coordinates tempCrd = findCellAround(fieldCell::Unknown, crd);
    while (tempCrd != errorCoordinates) {
        setCell(tempCrd, fieldCell::Empty);
        tempCrd = findCellAround(fieldCell::Unknown, crd);
    }

    tempCrd = findCellAround(fieldCell::Hit, crd);
    if (tempCrd != errorCoordinates)
        markKilled(tempCrd);
}

bool Field::canCellFitShip(const Coordinates crd, const unsigned size, const shipOrientation orientation) {
    if (size == 0)
        return true;

    if (getCell(crd) == fieldCell::Empty &&
        findCellAround(fieldCell::Ship, crd) == errorCoordinates) {

        if (orientation == shipOrientation::Horizontal)
            return canCellFitShip(crd.centralRight(), size - 1, orientation);
        else
            return canCellFitShip(crd.bottomCentral(), size - 1, orientation);
    }

    return false;
}

bool Field::setShipToCell(Coordinates crd, const unsigned size, const shipOrientation orientation) {
    if (size == 0)
        return true;
    if (!canCellFitShip(crd, size, orientation))
        return false;

    if (orientation == shipOrientation::Horizontal)
        for (int i = 0; i < size; i++)
            setCell(Coordinates(crd.x + i, crd.y), fieldCell::Ship);
    else
        for (int i = 0; i < size; i++)
            setCell(Coordinates(crd.x, crd.y + i), fieldCell::Ship);
}

Field::Field(fieldCell fillValue) {
    for (int i = 0; i < 10; i++)
        for (int j = 0; j < 10; j++)
            field[i][j] = fillValue;
}

Coordinates Field::getRandomCellOfType(const fieldCell target) const {
    //TODO: redo for better performance

    unsigned x, y;

    x = rand() % 10;
    y = rand() % 10;
    while (getCell(Coordinates(x, y)) != target) {
        x = rand() % 10;
        y = rand() % 10;
    }

    return Coordinates(x, y);
}
