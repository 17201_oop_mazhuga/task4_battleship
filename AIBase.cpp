//
// Created by 214 on 08.12.2018.
//

#include "AIBase.h"

void AIBase::start() {
    std::string serverInput;

    makeField();

    while (std::cin >> serverInput) {
        if (serverInput == "Win!" || serverInput == "Lose") {
            break;
        }
        else if (serverInput == "Arrange!")
            myField.print();
        else if (serverInput == "Shoot!") {
            while (serverInput != "Miss") {

                Coordinates shootCoordinates = shoot();
                std::cout << shootCoordinates << '\n';
                std::cin >> serverInput;

                if (serverInput == "Miss") {
                    enemyField.setCell(shootCoordinates, fieldCell::Empty);
                } else if (serverInput == "Hit") {
                    enemyField.setCell(shootCoordinates, fieldCell::Hit);
                } else if (serverInput == "Kill") {
                    enemyField.markKilled(shootCoordinates);
                }
            }
        }
    }
}

AIBase::AIBase() : myField(fieldCell::Empty), enemyField(fieldCell::Unknown){
    srand(time(NULL));
}
