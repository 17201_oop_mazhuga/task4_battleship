//
// Created by d214 on 10/12/18.
//

#include "AIRandomWithFinishing.h"

AIRandomWithFinishing::AIRandomWithFinishing() {
}


void AIRandomWithFinishing::makeField() {
    Coordinates crd;
    shipOrientation orient;
    unsigned shipCounter;

    for (unsigned size = 4; size >= 1; size--) {
        shipCounter = 0;

        while (shipCounter < 5 - size) {
            crd = myField.getRandomCellOfType(fieldCell::Empty);
            orient = (shipOrientation)(rand() % 2);

            if (myField.setShipToCell(crd, size, orient))
                shipCounter++;
        }
    }
}

Coordinates AIRandomWithFinishing::shoot() const{
    Coordinates crd = enemyField.findCell(fieldCell::Hit);
    if (crd != errorCoordinates)
        return finish(crd);

    return enemyField.getRandomCellOfType(fieldCell::Unknown);
}

Coordinates AIRandomWithFinishing::finish(const Coordinates crdHit, const shipOrientation orient) const {
    Coordinates crdTemp = enemyField.findCellAround(fieldCell::Hit, crdHit, Direction::LeftAndBottom);

    if (enemyField.getCell(crdHit.centralLeft()) == fieldCell::Unknown && crdTemp != crdHit.bottomCentral())
        return crdHit.centralLeft();
    if (enemyField.getCell(crdHit.topCentral()) == fieldCell::Unknown && crdTemp != crdHit.centralLeft())
        return crdHit.topCentral();

    if (crdTemp != errorCoordinates) {
        if (crdTemp == crdHit.centralRight())
            return finish(crdTemp, shipOrientation::Horizontal);
        else if (crdTemp == crdHit.bottomCentral())
            return finish(crdTemp, shipOrientation::Vertical);
    }

    if (orient == shipOrientation::Horizontal && enemyField.getCell(crdHit.centralRight()) == fieldCell::Unknown)
        return crdHit.centralRight();
    else if (orient == shipOrientation::Vertical && enemyField.getCell(crdHit.bottomCentral()) == fieldCell::Unknown)
        return crdHit.bottomCentral();
    else
        return enemyField.findCellAround(fieldCell::Unknown, crdHit, Direction::NoCorners);
}
